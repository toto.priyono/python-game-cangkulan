#
#  Cangkulan Version 1.0
#  By Toto Priyono
#  31/07/2021
#
#  Cangkul is a simple but popular Indonesian game, in which players try to get rid of cards by following suit
#  The first player has no card is the winner
#  The game is best for around 3 to 5 players. 
#  A standard international 52-card pack is used, the cards in each suit ranking from high to low A-K-Q-J-10-9-8-7-6-5-4-3-2
#  The dealer deals 7 cards to each player, and stacks the remainder of the pack face down to form a drawing stock. 
#  The players pick up their cards and look at them, without showing them to other players.
#



from random import shuffle
import os
import time
import math
import numpy as np

#-----------------------------------------------------------------------------
# Class  Card Game
# Careted 31/07/2021
# Author : Toto Priyono
#-----------------------------------------------------------------------------
class setupCard:
    def __init__(self):
        self.numberCard=[2,3,4,5,6,7,8,9,10]
        self.faceCard=["AS","K","Q","J"]
        self.symbolCard=["♠","♥","♦","♣"]
        self.playCard= setupCard.initCard(self)
        self.totalCard=len(self.playCard)
    
    # 
    # Function to prepare Card 
    # Normal Card have 52 pieces, then shuffle the card
    #    
    def initCard(self):
        normalCard=[]
        for x in self.symbolCard:
            for i in self.numberCard:
                normalCard.append([x,i,i])
            for j in self.faceCard:
                if (j=="AS"):
                    normalCard.append([x,j,14])
                elif(j=="K"):    
                    normalCard.append([x,j,13])
                elif(j=="Q"):
                    normalCard.append([x,j,12])
                else:
                    normalCard.append([x,j,11])
                    
        shuffle(normalCard)
        return normalCard
    
    #
    # Function to pick card from card on desk
    # Each Player will pick card when the player ddn't have card with the same symbol with card on table.
    # The Player will pick the card untul get card with same symbol
    #
    def getCard(self):
        popCard=self.playCard.pop()
        self.totalCard= len(self.playCard)
        return popCard


#-----------------------------------------------------------------------------
# Class Player Game
# Careted 31/07/2021
# Author : Toto Priyono
#-----------------------------------------------------------------------------
class playerGame:
    def __init__(self,playerID,playerName):
        self.id=playerID
        self.name=playerName
        self.cardOnHand=[]
        self.totalCard=0
        self.grabCardOnTable=False
    #
    # Function for pick Card from Card on Hand
    # To play , we must pick one card then put to the table
    #
    def selectCard(self,sel):
        selCard=self.cardOnHand[sel-1]
        return selCard
    #
    # Fucntion to print all card on player's Hand
    # This is to inform players what kind of card they have
    #
    def showCard(self):
        print("  "+self.name+".. , You have",self.totalCard,"Card")
        n=1
        strCard=""
        for card in self.cardOnHand:
            #print("Debug :",card)
            strCard+=str(n) + ": [" + card[0] + " " + str(card[1]) + "]  "
            n+=1
        #print("  This is your Cards :")
        print("  "+strCard)
    #
    # Function to Check whether the player has valid Card to play
    # You must have at least one card have the same Symbol with card on the table
    #
    def hasValidCard(self,initCard):
        validCard=False;
        if (len(initCard)==0):
            validCard=True
        else:
            for card in self.cardOnHand:
                if(card[0]==initCard[0]):
                    validCard=True       
        return validCard
    #
    # Function to check whether player has no card
    # if player has no card, it's mean the player is the winner
    #
    def hasEmptyCard(self):
        if(len(self.cardOnHand)==0):
            return True
        else:
            return False
        
    def drawPrettyCard(self):
        print("  "+self.name+".. , You have",self.totalCard,"Card")
        
        lineCard=math.ceil(len(self.cardOnHand)/10)
        #print(lineCard)
        cardOnHandSplit=np.array_split(self.cardOnHand,lineCard)
        #print(cardOnHandSplit)
        cardId=0
        for i in range(lineCard):
            for line in range(5):
                noCard=1
                for card in cardOnHandSplit[i]:
                    if(line==0):
                        cardId+=1
                        if(noCard==len(cardOnHandSplit[i])):
                            if(cardId>=9):
                                print(str(cardId)+".┌───────┐  ")
                            else:
                                print(str(cardId)+".┌───────┐   ")
                        else:
                            if(cardId>=9):
                                print(str(cardId)+".┌───────┐  ",end="")
                            else:
                                print(str(cardId)+".┌───────┐   ",end="")
                    if(line==1):
                        if(noCard==len(cardOnHandSplit[i])):
                            if(len(str(card[1]))==2):
                                print("  │"+str(card[1])+"     │   ")
                            else:
                                print("  │"+str(card[1])+"      │   ")
                        else:
                            if(len(str(card[1]))==2):
                                print("  │"+str(card[1])+"     │   ",end="")
                            else:
                                print("  │"+str(card[1])+"      │   ",end="")
                    if(line==2):
                        if(noCard==len(cardOnHandSplit[i])):
                            print("  │   "+card[0]+"   │   ")
                        else:
                            print("  │   "+card[0]+"   │   ",end="")
                                        
                    if(line==3):
                        if(noCard==len(cardOnHandSplit[i])):
                            if(len(str(card[1]))==2):
                                print("  │     "+str(card[1])+"│   ")
                            else:
                                print("  │      "+str(card[1])+"│   ")
                        else:
                            if(len(str(card[1]))==2):
                                print("  │     "+str(card[1])+"│   ",end="")
                            else:
                                print("  │      "+str(card[1])+"│   ",end="")
                    if(line==4):
                        if(noCard==len(cardOnHandSplit[i])):
                            print("  └───────┘   ")
                        else:
                            print("  └───────┘   ",end="")               
                    noCard+=1
            
#-----------------------------------------------------------------------------
# Class Table Game
# Careted 31/07/2021
# Author : Toto Priyono
#-----------------------------------------------------------------------------


class tableGame:
    def __init__(self,firstCard,playCard,gamePlayer):
        self.firstCard=firstCard
        self.playerCard=[]
        self.playCard=playCard
        self.player=gamePlayer
        self.tempWin=0
    
    #
    # Function to check player card
    # When Player put card on the table, the card must have the same symbol with existing card on the table
    #
    def checkValidCard(self,card):
        if (len(self.firstCard)>0):
            if card[0]==self.firstCard[0]:
                return True
            else:
                return False
        else:
            self.firstCard=card
            return True
    
    #
    # Function to display All card on the table game
    # Each player has put a card, and wil display on the table, so each player can see
    #
    def displayCard(self):
        print("\n  TABLE GAME")
        print("  ------------------------------------------------------\n")
        print("  Total Card on Deck =",len(self.playCard),"Card")
        if(len(self.firstCard)>0):
            print("  Init Card = "+"["+self.firstCard[0]+" "+str(self.firstCard[1])+"]")
        
        n=0
        for card in self.playerCard:
            print("  " + self.player[card[3]].name+ "'s Card = "+"["+ card[0] +" "+ str(card[1])+"]")
            #print("  Palyer ",self.player[card[3]].name," Card = ",card)
            if(n>=(len(self.player)-1)):
                idP=tableGame.whoTempWin()
                player=self.player[idP]
                print("\n  Ok,",player.name,"has the highest score, ")
                print("  Now",player.name,"will Play first..., Press Enter to Continue...")
                print("\n")
                print("  ------------------------------------------------------\n")
                input()
                #time.sleep(1.5)  
            n+=1
                
        print("\n")
        print("  ------------------------------------------------------\n\n")
        
    
    #
    # Function to check who is the winner on the iteration/session
    # the highest value of card is the winner, and then the player can play first at the next iteration/session
    #
    def whoTempWin(self):
        tempWin=0
        higestValue=0
        for card in self.playerCard: #example card=["♠","AS",11,1]
            if (card[2]>higestValue):
                higestValue=card[2]
                tempWin=card[3]
        return tempWin
    
    #
    # Function to identify the next player will play
    #    
    def getPlayerPlay(self,grabAll=False):
        
        if ((len(self.playerCard)==0) and (self.tempWin==0)):
            playerPlay=self.player[0]
            return playerPlay
        else:
            if(len(self.playerCard)==len(self.player)):
                self.tempWin=tableGame.whoTempWin()
                playerPlay=self.player[self.tempWin]
                self.firstCard=[]
                self.playerCard=[]
                return playerPlay
                
            elif(grabAll==True):
                self.tempWin=tableGame.whoTempWin()
                playerPlay=self.player[self.tempWin]
                self.playerCard=[]
                self.firstCard=[]
                return playerPlay
            else:
                self.tempWin+=1
                if (self.tempWin>(len(self.player)-1)):
                    self.tempWin=0
                playerPlay=self.player[self.tempWin]
                return(playerPlay)
                
                

#-----------------------------------------------------------------------------
# Display Title Game
# Clear Screen Before Play
#-----------------------------------------------------------------------------
def displayTitleGame():
    os.system('cls||clear')
    print("\n")
    print("  =====================================================")
    print("  |                                                   |")
    print("  |  Card Game Cangkulan Version 1.0                  |")                    
    print("  |  Final Project on Pirple Python Course            |")  
    print("  |  By Toto Priyono                                  |") 
    print("  |                                                   |")
    print("  =====================================================\n\n")              



def helpScreen():
    os.system('cls||clear')
    f=open("user_guide.txt","r")
    data=f.read()
    print(data)
    f.close()
    print("\n")
    print("Press Enter to Continue..")
    input()

#-----------------------------------------------------------------------------
#  Start Game
#  Display title and get number of player 
#-----------------------------------------------------------------------------

displayTitleGame()

#
# Input number players
# Error handling if input not numeric
#
numberPlayer=0
while ((int(numberPlayer)<2)or(int(numberPlayer)>5)or(numberPlayer=="--help")):
    numberPlayer=input("  How many players will play the Game ( 2-5 ) ? ")
    if(numberPlayer=="--help"):
        helpScreen()
        displayTitleGame()
        numberPlayer=0
    else:
        try:
            numberPlayer=int(numberPlayer)    
        except:
            print("  please enter numeric only or --help")
            numberPlayer=0

players=[]

#
# Prepare Game Player & Asking for each name
# Also Check the input name has different all
#
for i in range(1,numberPlayer+1):
    playerName=input("  Please input Player "+str(i)+" Name : ")
    alreadyExist=True
    while (alreadyExist)or(playerName=="--help"):
        alreadyExist=False
        if(playerName=="--help"):
            helpScreen()
            displayTitleGame()
            playerName=input("  Please input Player "+str(i)+" Name : ")
        else:    
            for p in players:
                if(playerName==p.name):
                    alreadyExist=True
                    playerName=input("  "+playerName+" already exist, Please input another name for Player "+str(i)+" : ")
                    
        if(alreadyExist==False):    
            player=playerGame(i-1,playerName)
            players.append(player)

#    
# Prepare Card for playing and give each player 7 card for initial game
#

card=setupCard()
for i in range(0,7):
    id=0
    for player in players:
        giveCard=card.getCard()
        giveCard.append(id)
        player.cardOnHand.append(giveCard)
        player.totalCard+=1
        id+=1
#
# Prepare Table Game and display Card on Table
# One card will open to initial
#

stratCardToPlay=card.getCard()
tableGame=tableGame(stratCardToPlay,card.playCard,players)
tableGame.displayCard()



#
# Start Game,
# Make Loop until one of the player has no Card
#
cardPlayerNotEmpty=True
iterationGamePlay=0
grabCardOnTable=False

while cardPlayerNotEmpty:
    
    for i in range(len(players)):        
        if(iterationGamePlay==0):
            player=players[0]
        else:
            player=tableGame.getPlayerPlay(grabCardOnTable)
            grabCardOnTable=False
        
        iterationGamePlay+=1
        
        displayTitleGame()
        tableGame.displayCard()
        
        time.sleep(0.5)
        #player.showCard()
        player.drawPrettyCard()
        notValidCard=True

        #
        #  If player have valid Card to Play
        #  Player can pick/select one card from card on his/her hand.
        #
        
        hasValidCard=player.hasValidCard(tableGame.firstCard)
        
        if(hasValidCard):
            while notValidCard:
                if(len(tableGame.firstCard)==0):
                    sel=0
                    while((int(sel)<1) or (int(sel)>len(player.cardOnHand))or(sel=="--help")):
                        sel=input("\n  Please Select your card to Play [1-"+ str(len(player.cardOnHand)) + "] ? ")
                        if(sel=="--help"):
                            helpScreen()
                            displayTitleGame()
                            tableGame.displayCard()
                            time.sleep(0.5)
                            player.drawPrettyCard()
                            sel=0
                        else:
                            try:
                                sel=int(sel)
                            except ValueError:
                                sel=0   
                else:
                    sel=0
                    while((sel<1) or (sel>len(player.cardOnHand))):                 
                        sel=input("\n  Please Select your card ["+tableGame.firstCard[0]+"] to Play ? ")
                        if(sel=="--help"):
                            helpScreen()
                            displayTitleGame()
                            tableGame.displayCard()
                            time.sleep(0.5)
                            player.drawPrettyCard()                            
                            sel=0
                        else:
                            try:
                                sel=int(sel)
                            except ValueError:
                                sel=0      
                    
                selectedCard=player.cardOnHand[sel-1]
                if(tableGame.checkValidCard(selectedCard)):
                    notValidCard=False
                    player.cardOnHand.pop(sel-1)
                    player.totalCard-=1
                    tableGame.playerCard.append(selectedCard)
                else:
                    notValidCard=True
                    print("  Your Card not Valid, please another card!")
            
            if(player.hasEmptyCard()):
                cardPlayerNotEmpty=False
                print("\n\n  GAME OVER, The Winner is",player.name)
                break
        #
        # If player don't have valid card
        # Player must take card from card on the deck until get valid card ( card with same symbol)
        #
        else:
            
            #
            # Player can take card if card on desk not empty
            #
            if(card.totalCard>0):
                print("\n  Unfortunately.. you don't have valid Card on your Hand")
                print("  You must take Card form the Deck")
                input("  Press Enter to Continue..")

                #
                # Player will take card on the deck until get valid card
                # If card on the deck empty and stil not get valid card, player must take all card on the table
                #
                notValidCard=True
                while (hasValidCard==False):
                    if(card.totalCard>0):
                        giveCard=card.getCard()
                        giveCard.append(player.id)
                        player.cardOnHand.append(giveCard)
                        player.totalCard+=1
                        hasValidCard=player.hasValidCard(tableGame.firstCard)
                    else:
                        print("\n  You don't have valid Card on your Hand")
                        print("  Unfortunately.. there is no Card on the Deck")
                        print("  You Must Take all card on the Table..")
                        input("  Press Enter to Continue..\n\n")
                        #
                        #  Take all card on table to add card on player's hand
                        #  Make sure to change the player.id on the card
                        # 
                        tempGrabCard=[]
                        for eachCard in tableGame.playerCard:
                            tempGrabCard=eachCard.copy()
                            tempGrabCard[3]=player.id
                            player.cardOnHand.append(tempGrabCard)
                            
                        player.totalCard=len(player.cardOnHand)
                        grabCardOnTable=True
                        notValidCard=False
                        break
            
                if(grabCardOnTable==False):
                    print("\n  After take Card ...")
                    print("  Now, you already have valid card to play..")
                    player.drawPrettyCard()
                
                    while notValidCard:
                        if(len(tableGame.firstCard)==0):
                            sel=0
                            while((int(sel)<1) or (int(sel)>len(player.cardOnHand))or(sel=="--help")):
                                sel=input("\n  Please Select your card to Play [1-"+ str(len(player.cardOnHand)) +"] ? ")
                                if(sel=="--help"):
                                    helpScreen()
                                    displayTitleGame()
                                    tableGame.displayCard()
                                    time.sleep(0.5)
                                    player.drawPrettyCard()                            
                                    sel=0
                                else:
                                    try:
                                        sel=int(sel)
                                    except ValueError:
                                        sel=0                                
                        else:
                            sel=0
                            while((int(sel)<1) or (int(sel)>len(player.cardOnHand))or(sel=="--help")):
                                sel=input("\n  Please Select your card ["+tableGame.firstCard[0]+"] to Play ? ")
                                if(sel=="--help"):
                                    helpScreen()
                                    displayTitleGame()
                                    tableGame.displayCard()
                                    time.sleep(0.5)
                                    player.drawPrettyCard()                            
                                    sel=0
                                else:
                                    try:
                                        sel=int(sel)
                                    except ValueError:
                                        sel=0   
                                    
                        selectedCard=player.cardOnHand[sel-1]
                        if(tableGame.checkValidCard(selectedCard)):
                            notValidCard=False
                            player.cardOnHand.pop(sel-1)
                            player.totalCard-=1
                            tableGame.playerCard.append(selectedCard)
                        else:
                            notValidCard=True
                            print("  Your Card not Valid, please another card!")
                
                    if(player.hasEmptyCard()):
                        cardPlayerNotEmpty=False
                        print("\n\n  GAME OVER, The Winner is",player.name)
                        print("  Congtarulation..",player.name)
                        break
                else:
                    break    
                             
            #
            # If card on desk is empty, player must take all card on the table
            # and first player will play again
            #
            else:
                print("\n  You don't have valid Card on your Hand")
                print("  Unfortunately.. there is no Card on the Deck")
                print("  You Must Take all card on the Table..")
                input("  Press Enter to Continue..\n\n")
                #
                #  Take all card on table to add card on player's hand
                #  Make sure to change the player.id on the card
                # 

                tempGrabCard=[]
                for eachCard in tableGame.playerCard:
                    tempGrabCard=eachCard.copy()
                    tempGrabCard[3]=player.id
                    player.cardOnHand.append(tempGrabCard)
                  
                player.totalCard=len(player.cardOnHand)
                grabCardOnTable=True
                break
        
        displayTitleGame()
        tableGame.displayCard()
       

    

     
        
    